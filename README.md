# Device Management Profile

## DNSSettings

The payload for configuring encrypted DNS settings.

Availability

* iOS 14.0+
* iPadOS 14.0+
* macOS 11.0+

## Notes

Download one of the below DNS Profiles to your iOS or macOS machine (preferable via Safari browser), and setup the encrypted DNS setting on your device. Based on [apple devloper notes](https://developer.apple.com/documentation/devicemanagement/dnssettings).

## LibreDNS DoH

* [LibreDNS-DoH.mobileconfig](https://gitlab.com/libreops/libredns/device-management-profile/-/raw/master/LibreDNS-DoH.mobileconfig)
* [LibreDNS-(noads)-DoH.mobileconfig](https://gitlab.com/libreops/libredns/device-management-profile/-/raw/master/LibreDNS-noads-DoH.mobileconfig)

## LibreDNS DoT

* [LibreDNS-DoT.mobileconfig](https://gitlab.com/libreops/libredns/device-management-profile/-/raw/master/LibreDNS-DoT.mobileconfig)
* [LibreDNS-(noads)-DoT.mobileconfig](https://gitlab.com/libreops/libredns/device-management-profile/-/raw/master/LibreDNS-noads-DoT.mobileconfig)
